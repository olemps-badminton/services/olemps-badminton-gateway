FROM gradle:jdk17 as builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle bootJar --no-daemon

FROM openjdk:17-alpine
COPY --from=builder /home/gradle/src/build/libs/olemps-badminton-gateway.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

EXPOSE 8080
